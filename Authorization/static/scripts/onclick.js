let xhr = new XMLHttpRequest();
let token = null
let refresh_token = null

function goToIndex() {
    xhr.open("GET", "/login", false)
    xhr.send()
    var ans = JSON.parse(xhr.responseText)
    let body = document.createElement('body')
    body.innerHTML = ans.body
    document.body.replaceWith(body)
}

function updateToken() {
    xhr.open("GET", "/updateToken", false)
    xhr.setRequestHeader("RefreshToken", refresh_token)
    xhr.send()
    var ans = JSON.parse(xhr.responseText)
    if (ans.err_code == 0) {
        token = ans.token
        return true
    }
    return false
}

function getSessions() {
    xhr.open("GET", "/sessions", false)
    xhr.setRequestHeader('Token', token);
    xhr.send()
    var ans = JSON.parse(xhr.responseText)
    if (ans.err_code == 1) {
        if (updateToken()) {
            getSessions()
        } else {
            goToIndex()
        }
    } else {
        let body = document.createElement('body')
        body.innerHTML = ans.body
        document.body.replaceWith(body)
    }
}

function login() {
    xhr.open("POST", "/login", false)
    var login = document.getElementById("login")
    var pass = document.getElementById("pass")
    xhr.send(JSON.stringify({ "login": login.value, "password": pass.value }))
    let ans = JSON.parse(xhr.responseText)
    if (ans.err_code == 0) {
        token = ans.token
        refresh_token = ans.refresh_token
        getSessions()
    } else {
        let err = document.createTextNode("wrong!")
        document.body.append(err)
    }
}

function goToRegistration() {
    xhr.open("GET", "/signup", false)
    xhr.send()
    let ans = JSON.parse(xhr.responseText)
    let body = document.createElement('body')
    body.innerHTML = ans.body
    document.body.replaceWith(body)
}

function signUp() {
    xhr.open("POST", "/signup", false)
    var login = document.getElementById("login")
    var pass = document.getElementById("pass")
    xhr.send(JSON.stringify({ "login": login.value, "password": pass.value }))
    let ans = JSON.parse(xhr.responseText)
    if (ans.err_code == 0) {
        goToIndex()
    } else {
        let err = document.createTextNode("Choose alternative login")
        document.body.append(err)
    }
}

function getSession(id) {
    xhr.open("GET", "/sessions/" + id, false)
    xhr.send()
    ans = JSON.parse(xhr.responseText)
    if (ans.err_code == 0) {
        let body = document.createElement('body')
        body.innerHTML = ans.body
        document.body.replaceWith(body)
    } else {
        if (updateToken()) {
            getSession(id)
        } else {
            goToIndex()
        }
    }
}
