import websockets
import asyncio
import logging


class SessionProcessor:
    def __init__(self):
        self.session_to_clients = {}
        self.current_session_id = None

    async def process(self, websocket):
        while True:
            msg = await websocket.recv()
            if msg == "session_id":
                self.current_session_id = msg
                self.session_to_clients[msg] = self.session_to_clients.get(msg, set())
                self.session_to_clients[msg].add(websocket)
                logging.info(f"Success")
            else:
                for client in self.session_to_clients.get(
                    self.current_session_id, set()
                ):
                    if client != websocket:
                        logging.info(f"Sent message to {client}")
                        await client.send(msg)

    async def run(self):
        async with websockets.serve(self.process, "localhost", 8080) as websocket:
            await asyncio.Future()


sp = SessionProcessor()
asyncio.run(sp.run())
