import datetime


active_token_time = datetime.datetime.now()
refersh_token_time = datetime.datetime.now()


def valid_token(token):
    global active_token_time
    if datetime.datetime.now() - active_token_time > datetime.timedelta(seconds=30):
        active_token_time = datetime.datetime.now()
        return False
    return True


def valid_refresh_token(token):
    global active_token_time
    if datetime.datetime.now() - refersh_token_time > datetime.timedelta(seconds=90):
        active_token_time = datetime.datetime.now()
        return False
    return True
