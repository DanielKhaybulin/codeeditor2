from flask import (
    Flask,
    request,
    session,
    render_template,
    redirect,
    url_for,
    make_response,
)
import json
from dataclasses import dataclass
from tokens import valid_token, valid_refresh_token
import logging


@dataclass
class Session:
    session_id: str
    session_name: str


TOKEN = "my_secret_token"

handler = Flask(__name__)


def generate_token():
    return "my_secret_token"


@handler.route("/", methods=["GET"])
def starting_page():
    return render_template("index.html")


@handler.route("/updateToken", methods=["GET"])
def update_token():
    if request.headers.get("RefreshToken"):
        if valid_refresh_token(request.headers.get("RefreshToken")):
            return (
                json.dumps({"err_code": 0, "token": generate_token()}),
                200,
                {"Content-Type": "application/json"},
            )
    return (
        json.dumps({"err_code": 1, "token": ""}),
        200,
        {"Content-Type": "application/json"},
    )


@handler.route("/login", methods=["GET", "POST"])
def login_page():
    if request.method == "POST":
        data = request.get_data()
        data = json.loads(data)
        ans = {
            "err_code": 0,
            "token": generate_token(),
            "refresh_token": generate_token(),
        }
        return json.dumps(ans), 200, {"Content-Type": "application/json"}
    else:
        return (
            json.dumps({"body": render_template("starting.html")}),
            200,
            {"Content-Type": "application/json"},
        )


@handler.route("/signup", methods=["GET", "POST"])
def sign_up():
    if request.method == "GET":
        return (
            json.dumps({"body": render_template("signup.html")}),
            200,
            {"Content-Type": "application/json"},
        )
    else:
        return (
            json.dumps({"err_code": 0}),
            200,
            {"Content-Type": "application/json"},
        )


@handler.route("/sessions", methods=["GET", "POST"])
def sessions_choice():
    if request.headers.get("Token"):
        if valid_token(request.headers.get("Token")):
            if request.method == "GET":
                sessions = [Session("1234", "first"), Session("345", "second")]
                return (
                    json.dumps(
                        {
                            "err_code": 0,
                            "body": render_template(
                                "sessions.html", available_sessions=sessions
                            ),
                        }
                    ),
                    200,
                    {"Content-Type": "application/json"},
                )
            else:
                return "", 200, {"Content-Type": "application/json"}
    return (
        json.dumps({"err_code": 1, "body": ""}),
        401,
        {"Content-Type": "application/json"},
    )


@handler.route("/sessions/<session_id>")
def enter_session(session_id):
    if valid_token(request.headers.get("Token")):
        return (
            json.dumps(
                {"err_code": 0, "body": render_template("session.html", id=session_id)}
            ),
            200,
            {"Content-Type": "application/json"},
        )
    return (
        json.dumps({"err_code": 1, "body": ""}),
        401,
        {"Content-Type": "application/json"},
    )


if __name__ == "__main__":
    handler.debug = True
    handler.run()
